class LogModel {
    constructor(transId = new Date().getTime() , requester = "test") {
        this.transId = transId;
        this.requester = requester;
        this.endpoint = "";
        this.request = "";
        this.response = "";
        this.code = "";
        this.elapsedTime = 0;
        this.stack;
    }

    setRequest(endpoint, request, time) {
        this.clear();
        if (time) {
            this.elapsedTime = time;
        } else {
            this.elapsedTime = new Date().getTime();
        }
        this.endpoint = endpoint;
        this.request = JSON.stringify(request);
    }

    setResponse(response, code) {
        this.elapsedTime = new Date().getTime() - this.elapsedTime;
        this.response = JSON.stringify(response);
        this.code = code;
    }

    setStack(stack) {
        this.elapsedTime = new Date().getTime() - this.elapsedTime;
        this.stack = JSON.stringify(stack);
    }

    clear() {
        this.response = "";
        this.code = "";
        this.stack = undefined;
        this.elapsedTime = 0;
    }
}
module.exports = LogModel;