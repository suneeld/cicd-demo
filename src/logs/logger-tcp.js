var winston = require("winston");
var fluentTransport = require("fluent-logger").support.winstonTransport();

var config = {
  host: "172.19.219.223",
  // port: 24229,
  port: 24225,
  timeout: 3.0
};
var logger = winston.createLogger({
  transports: [
    new fluentTransport("nodejs-wingston", config),
    new winston.transports.Console({ timestamp: true })
  ]
});

logger.on("logging", (transport, level, message, meta) => {
  if (meta.end && transport.sender && transport.sender.end) {
    transport.sender.end();
  }
});

module.exports = logger;