var express = require("express");
var config = require("../config/config");
var router = express.Router();
var http = require("../services/httpService");
var packageJson = require("../../package.json");
var LogModel = require("../logs/log-model");
var fs = require("fs");

var path = require("path");

var os = require("os");
// var db = require('./../services/db-service');

/* GET home page. */
router.get("/", function(req, res) {
  let resp = {
    host: os.hostname(),
    env: process.env.env || "dev",
    version: packageJson.version
  };
  res.json(resp);
});

router.get("/showconfig", function(req, res) {
  // try {
    // var cmpath = path.join(__dirname, "../../config/configfile.json");

    // var cmfile = fs.readFileSync(cmpath);
  //   cmfile = JSON.parse(cmfile);
  // } catch (err) {
  //   console.log(err);
    var cmfile = "no configmapfile";
  // }
  res.json({ etcd: config, env: process.env, cmfile: cmfile });
});

router.get("/get", async (req, res, next) => {
  try {
    var logmodel = new LogModel("11111111111", "222222222222");

    var ret = await http.get(
      "https://jsonplaceholder.typicode.com/todos",
      "_limit=5",
      logmodel
    );

    res.json(ret);
  } catch (err) {
    console.error(err.code);
    res.json({ error: 500 });
  }
});

// router.get("/query", (req, res, next) => {
//   try {
//     db.get();
//     res.json("query");
//   } catch (err) {
//     console.error(err.code);
//     res.json({ error: 500 });
//   }
// });

module.exports = router;
