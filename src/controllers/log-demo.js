var express = require("express");
var router = express.Router();
var log = require("./../logs/logger");
var http = require('../services/httpService');
var LogModel = require('../logs/log-model');

router.get("/", async function (req, res, next) {

    // http://localhost:3000/log?trans=1234&reqester=0828282828&limit=3
    var { trans, reqester, limit } = req.query;

    var logmodel = new LogModel(trans, reqester);

    if(!limit){
        limit = 1;
    }
    var ret = await http.get('https://jsonplaceholder.typicode.com/todos', "_limit=" + limit, logmodel);


    logmodel.setRequest("/log", req.query);
    logmodel.setResponse(ret.length, "200");
    log.info("seccess", logmodel);

    res.json(ret.length);
});

module.exports = router;
